//
//  AppDelegate.swift
//  Melghat
//
//  Created by Pawan Ramteke on 05/03/20.
//  Copyright © 2020 Pawan Ramteke. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

