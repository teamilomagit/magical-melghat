//
//  ViewController.swift
//  Melghat
//
//  Created by Pawan Ramteke on 05/03/20.
//  Copyright © 2020 Pawan Ramteke. All rights reserved.
//

import UIKit
import WebKit
class ViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        
        //Melghat url = https://magicalmelghat.studentduniya.com/
        super.viewDidLoad()
        
        webView.load(URLRequest(url: URL(string: "https://m.magicalmelghat.com/")!))
        webView.scrollView.bounces = false
        webView.scrollView.delegate = self
        
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
         scrollView.pinchGestureRecognizer?.isEnabled = false
    }


}

